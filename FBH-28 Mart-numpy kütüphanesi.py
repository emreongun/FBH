﻿#-----------------------------------------------------------28.04.2015---9.hafta

import numpy as np
çizgi="------------------------------------------------------------"
q=np.array([[1,2],[3,4]],float) # float yazılmak zorunda değil
w=np.array([[5,6],[7,8]],float) # array idadesi bir çeşit dizi tanımlıyor.
print("q dizisi şöyle bir yapıya sahip:\n",q) # '\n' ifadesi yeni satır komutu
print("w dizisi de şu elemanlara sahip:\n",w)
print("q dizisinin biçimi: ",q.shape) # biçimini gösteriyor
print("w dizisini 4×1 matris biçimine çevirme:\n ",w.reshape(4,1)) # 4 elemanlı 1 dizi
print("w dizisini 1×4 matris biçimine çevirme:\n ",w.reshape(1,4)) # 1 elemanlı 4 dizi
# w.reshape(n,m) ifadesi w dizisini n elemanlı m adet alt diziden oluşucak şekilde yeniden biçimlendirir
# n*m = len(w) olmalı. Yani yeniden biçimlendirilen dizinin eleman sayısı dikkate alınmalı
print("İz(q)+İz(w) =",q.trace()+w.trace()) # izlerin toplamı
print(çizgi)

e=np.ones_like(q) # elemanlarının hepsi 1 olan q'ya benzer yapıda dizi
h=np.ones((2,5),int) # (2,5) biçiminde elemanları 1 olan dizi
r=np.zeros_like(w) # elemanlarının hepsi 0 olan w'ya benzer yapıda dizi
j=np.zeros(10,int) # elemanları 0 olan 10 elmanlı dizi
print("e dizisi: elemanları 1",e,"r dizisi: elemanları 0",r,sep="\n") # bu dizileri yazdırıp neye benzediklerine bakalım
print("h dizisi: ",h,"j dizisi: ",j,sep="\n")
t=np.identity(2,float) # 2'ye 2'lik birim matris gibi dizi tanımlar
print("t dizisi: birim dizi: birim matris\n",t)
y=(e+r+t).copy() # toplamların kopyasını y'ye atıyor
print("y dizisi: e+r+t \n",y)
y.fill(3) # bütün elemanları 3 yapar
print("y dizisi: elemanları 3 \n",y)
print(çizgi)

u=q.transpose();ı=w.transpose() # transpozelerini alıyor
print("u dizisi (matrisi): transpoze q",u,"ı dizisi (matrisi): transpoze w",ı,sep="\n")
print("u dizisinin aritmetik ortası: ",np.mean(u))
print("ı dizisinin elmanları çarpımı: ",np.prod(ı))
print("u dizisinin elemanları toplamı: ",np.sum(u))
print("ı'nın en büyük elemanı: ",np.max(ı),"\nı'nın en büyük eleman adresi: ",np.argmax(ı)) # en büyük eleman ve adresi
print(çizgi)

o=np.concatenate((q,w)) # iki matrisin birleştirilmiş hali
print("o dizisi (matrisi): axis belirtilmeden \n",o)
p=np.concatenate((q,w),axis=0) # q matrisine w'nin satırlarını ekleyerek birleştir
print("p dizisi (matrisi): axis=0 \n",p)
ğ=np.concatenate((q,w),axis=1) # q matrisine w'nin sütünlarını ekleyerek birleştir
print("ğ dizisi (matrisi): axis=1 \n",ğ)
print("o dizisinin düzleştirilmiş hali: ",o.flatten())
print(çizgi)

a=np.array([[[11,12],[13,14]],[[15,16],[17,18]],[[19,20],[21,22]]],int) # üç adet matristen oluşuyor gibi
s=np.array([[[31,32],[33,34]],[[35,36],[37,38]],[[39,40],[41,42]]],int)
print("a dizisi:\n",a)
print("a dizisinin (tensörünün) eleman sayısı: ",len(a))
print("a dizisinin yapı biçimi: ",a.shape)
print("a dizisinin 2×2×3 biçimi:\n",a.reshape(2,2,3))
d=np.concatenate((a,s),axis=0) # olduğu gibi elemanları alt alta ekleyerek birleştirir: 3 matris + 3 matris daha
f=np.concatenate((a,s),axis=1) # matrisleri satır olarak birleştirir
g=np.concatenate((a,s),axis=2) # matrisleri sütün olarak birleştirir
# her birleşimde yeni bir 3. düzey tensör oluşur. (matrisler 2. düzey tensördür)
# axis ifadesi oluşturulan tensörün düzeyine kadar sayı alabilir

print("""
a,s,d,f,g dizileri (tensörleri):
a dizisi (tensörü)
{a}

s dizisi (tensörü)
{s}

d dizisi (tensörü) (a,s) birleşimi,axis=0
{d}

f dizisi (tensörü) (a,s) birleşimi,axis=1
{f}

g dizisi (tensörü) (a,s) birleşimi,axis=2
{g}
""".format(a=a,s=s,d=d,f=f,g=g),çizgi)

k=np.arange(24,dtype=int) # "for i in range(13):k.append(i)" komutuna denk
print("k dizisi: ",k)
print("k dizisi biçimi: ",k.shape)
print("k dizisi (2,3,4) biçimi:\n",k.reshape(2,3,4))
l=k.copy();l.fill(5)
ş=k.copy();ş.fill(3)
print("l dizisi: ",l,"\nş dizisi: ",ş)
print(" ","l dizisi (2,2,6) biçimi:",l.reshape(2,2,6),"\nş dizisi (2,2,6) biçimi:",ş.reshape(2,2,6),sep="\n")
l=l.reshape(2,2,6);ş=ş.reshape(2,2,6)
print(" ","l*ş işlem sonucu:",l*ş,sep="\n")
print(çizgi)

#y=np.array([7,8,9]);u=y.T # .T transpoze alıyormuş
#print("y dizisi:",y,"u dizisi:",u,"ikisinin çarpımı: -birim dizi (matris) olmalı-",y*u,sep="\n")
#print("q dizisinin (matrisinin) determinantı: ",np.linalg.det(q))
#print(q*np.linalg.inv(q)) # matrisin tersini alır

#özdeğer,özvektör=np.linalg.eig(q)
#print(özdeğer,özvektör,sep="\n")
