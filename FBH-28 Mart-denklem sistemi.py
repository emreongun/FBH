﻿#-----------------------------------------------------------28.04.2015---9.hafta

import numpy as np

# Aşağıdaki denklem sistemini çöz
# 3x+5y+3z = 22
# x-y-z = -4
# 3x-y+2z = 7

# [A]*[X]=[C] bunların hepsi matris biçiminde

A=np.array([[3,5,3],[1,-1,-1],[3,-1,2]])
B=np.array([22,-4,7])
print("A matrisi:",A,"","B matrisi:",B,"",sep="\n")

C=np.transpose(B) # C=B.transpose() olarak da yazılabilir
print("C matrisi:",C,"",sep="\n")

print("A matrisinin biçimi: ",A.shape)
print("B matrisinin biçimi: ",B.shape)
print("C matrisinin biçimi: ",C.shape)

X=np.dot(np.linalg.inv(A),C).reshape(3,1) # C yerine B yazınca da doğru sonucu veriyor
print("X matrisinin çözümü:\n",X)#.reshape(3,1))
print("X matrisinin biçimi: ",X.shape)

print("Aslında C matrisini şöyle yazdırmalıyız:")
C=B.reshape(3,1)
print(C)
print("Şimdiki C matrisinin biçimi: ",C.shape)
X=np.dot(np.linalg.inv(A),C) # C yerine B yazınca da doğru sonucu veriyor
print("Şimdiki X matrisinin çözümü:\n",X.reshape(3,1))
print("Yine doğru çözüm")
