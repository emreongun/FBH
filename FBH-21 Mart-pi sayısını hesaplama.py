﻿#-----------------------------------------------------------21.04.2015---8.hafta

# r yarıçaplı kürenin hacmi 4/3*pi*r**3
# birim küb içinde bir yarı-çeyrek (1/8) küre olduğunu düşünelim
# birim küb ve içindeki yarı-çeyrek kürenin hacimlerini kullanarak pi'yi bul...

import random
parça_küre=0 # parça kürede kaç nokta var, bilmiyoruz şimdilik
küp=100000 # küpteki nokta sayısı

for i in range(küp): 
    # küpteki her bir nokta x,y,z koordinatlarına sahiptir
    # rastgele noktalar
    x=random.random() 
    y=random.random()
    z=random.random()
    
    if (z**2+x**2+y**2 <= 1): # bu noktanın yer vektörü boyu kürenin yarı çapından kısa ise 
        parça_küre += 1  # nokta küreye dahildir

#pi/6=parça_küre hacmi / küp hacmi
pi=6*parça_küre/küp
print("pi = {0:.4f}".format(pi))



