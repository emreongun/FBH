﻿#-----------------------------------------------------------14.04.2015---7.hafta

import random # rastgele sayılar için kütüphane
def zar(atış):
    bir=iki=üç=dört=beş=altı=0 # zar atışlarında bu sayıların gelme sayısı
    for i in range(0,atış):
        r=int(random.randint(1,6)) # [1,6] aralığında rastgele tam sayı
        if r == 1 : bir += 1 # kaç kez "bir" geldiğini kaydeder
        elif r == 2 : iki += 1
        elif r == 3 : üç += 1
        elif r == 4 : dört += 1
        elif r == 5 : beş += 1
        else: altı += 1
    return bir,iki,üç,dört,beş,altı # altı adet çıktı üretiyor

for atış in range(10,201,10): # atış sayısı 10,20,...,200 atışta ayrı ayrı
    bir,iki,üç,dört,beş,altı=zar(atış)

    # olasılıklarını hesaplamak için "atış"a bölüyorum.
    rapor="""
    {0:3d} atışta 
    {1:3d} defa "1"
    {2:3d} defa "2"
    {3:3d} defa "3"
    {4:3d} defa "4"
    {5:3d} defa "5"
    {6:3d} defa "6" 
""".format(atış,bir,iki,üç,dört,beş,altı)
    print(rapor)
