﻿#-----------------------------------------------------------21.04.2015---8.hafta

import random
b=[] # oluşan her bir toplamın olasılığını barındıracak liste
for i in range(0,13):
    b.append(0.0) # bu olasılıkların hesaplamadan önce sıfıra eşitledik

atış=10000 # zarların atış sayısı
for j in range(0,atış):
    zar1=random.randint(1,6) # [1,6] aralığında tam sayı
    zar2=random.randint(1,6)
    j=zar1+zar2 # gelen değerlerin toplamı
    b[j]+=1 # b[j],j toplam değerinin geliş sayısı olucak
            # her j toplamı oluştuğunda b[j] bir artıcak

for i in range(2,13): # toplamların 0 ve 1 çıkma olasılığı olmadığından 2'den başlattım
    print("Toplamların {:2d} çıkma olasılığı %{:2.2f}".format(i,100*b[i]/atış))
