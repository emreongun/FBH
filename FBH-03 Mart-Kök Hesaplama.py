#-----------------------------------------------------------24.03.2015---3.Hafta

import math

a=1;b=-8;c=15
delta=b**2-4*a*c

if b<0:
    işlem1="-"
else:
    işlem1="+"

if c<0:
    işlem2="-"
else:
    işlem2="+"

print("Örnek işlev (fonksiyon): ",a,"x^2",işlem1,abs(b),"x",işlem2,abs(c),sep="")

if(delta>0):
    x1=(-b+math.sqrt(delta))/(2*a)
    x2=(-b-math.sqrt(delta))/(2*a)
    print("Kökler x1=",x1,"x2=",x2)
elif(delta==0):
    x=(-b)/(2*a)
    print("Çakışık kök:",x)
else:
    print("Reel kök yok")
