#-----------------------------------------------------------24.03.2015---6.Hafta

#İşlevler 'def' ifadesi ile tanımlanır
def işlem(liste):
    liste.append([1,2,3,4])
    print("'işlem' işlevinin içindeyiz.",liste)
    return # işlev kapsamı burada biter.

liste = [7,8,9]
işlem(liste)
print("'işlem' işlevinin dışındayız.",liste)
