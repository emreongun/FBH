﻿#-----------------------------------------------------------24.03.2015---6.Hafta

import math # matematik kütüphanesi

# işlem adındaki işlev (fonksiyon) toplama,çıkarma ve çarpma yapabiliyor.
def işlem(A,B,işleç="*"): # 'işlem' belirtilmezse çarpma yapılacağını varsayıyor
    C=[]        # sonuç matrisimiz
    n=len(A)    # A matrisinin satır sayısı
    l=len(A[0]) # A matrisinin -ilk satırının- sütün sayısı
                # B matrisinin satır sayısı da n olmalı
    m=len(B[0]) # B matrisinin sütün

    if işleç=="+" or işleç=="-":# toplama ve ya çıkarma ise
        for i in range(l):      # i+1. satır
                                # i=0 için matrisin i+1=1. satırındayız
            satır=[]            # her satır önce boş bir dizi olmalı
            for j in range(n):  # j+1. sütun  
                if işleç=="+":
                    sayı=A[i][j]+B[i][j] # i+1,j+1 elemanı
                elif işleç=="-":
                    sayı=A[i][j]-B[i][j]
                satır.append(sayı)       # satır elemanına ekle
            C.append(satır)              # matrise ekle

    elif işleç=="*":              # çarpma işlemi
        for i in range(n):        # n: A matrisinin satır sayısı
            satır=[]
            for j in range(m):    # m: B'nin sütun sayısı
                sayı=0            # her döngü başında sıfırlanmalı
                for k in range(l):# l: A'nın sütun, B'nin satır sayısı
                    sayı += A[i][k]*B[k][j]
                satır.append(sayı)
            C.append(satır)

    else:
        print("Tanımsız işleç: ",işleç)

    return C

# Matris işlemlerini güzel bir görüntüde yansıtması için
def İşlemGöster(A,B,işleç='*'):
    C=işlem(A,B,işleç) # işlem sonucu hesaplanıyor
    for satır in range(len(C)):
        if satır == int(len(C)/2):
            print(A[satır],işleç,B[satır],"=",C[satır])
        else:
            print(A[satır],B[satır],C[satır],sep="   ")
    print(" ")
    return 

# Örnek matris işlemi
# Matrisler eşit boyutta olmalı.
M1=[[1,1,1,1,1],[2,2,2,2,2],[3,3,3,3,3],[4,4,4,4,4],[5,5,5,5,5]]
M2=[[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5]]
işleç="+"
M3=işlem(M1,M2,işleç)
İşlemGöster(M1,M2,işleç)

# Bu sefer matris çarpımına örnek
# Matrislerin boyutu nxl'ye lxm olmalı
Q=[[1,1,1],[2,2,2],[3,3,3]]
W=[[1],[2],[3]]
E=işlem(Q,W)
İşlemGöster(Q,W)
