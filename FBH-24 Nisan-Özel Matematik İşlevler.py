﻿#-----------------------------------------------------------24.03.2015---6.hafta

def faktoriyel(sayı=1): # öntanımlı 'sayı', '1'e eşit
    if (sayı==1 or sayı==0):
        return 1
    else:
        return sayı*faktoriyel(sayı-1)

def faktoriyel_(sayı=1):
    if (sayı==1 or sayı==0):
        return 1
    else:
        a,b=1,2
        while b <= sayı:
            a *= b
            b += 1
        return b
            

def fibonakki(n=1):
    if (n==1 or n==2):
        return 1
    else:
        return fibonakki(n-1) + fibonakki(n-2)

def fibonakki_(n=1):
    if (n==1 or n==2):
        return 1
    else:
        a,b=0,1
        while b <= n:
            a,b=b,b+1
        return 1

işlem=input("""İşleminizi yazın: 
faktoriyel, kombinasyon ve ya fibonakki?: """)
# Şimdilik sadece faktoriyel ve kombinasyon hesaplatıyorum.

if işlem=="faktoriyel" or işlem=="faktoriyel_":
    x=int(input("Kaç faktoriye?: "))
    sonuç=faktoriyel(x)

elif işlem=="kombinasyon":
    n=int(input("Kaçın?: "))
    r=int(input("Kaça?: "))
    f=faktoriyel_
    sonuç=f(n)/(f(r)*f(n-r))

elif işlem=="fibonakki" or işlem=="fibonakki_":
    x=int(input())
    sonuç=fibonakki(x)

else:
    sonuç="bilinmiyor"

print(işlem,"işleminin sonucu:",sonuç)
