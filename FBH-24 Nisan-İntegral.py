﻿#-----------------------------------------------------------24.03.2015---6.Hafta

from math import *

def integral(f,a,b,n): # a'dan b'ye f intefrali
    if a>b or n<1:
        print("Hatalı veri")
    else:
        dx=(b-a)/n # delta_x
        s=0       # başlangıç alanı sıfır
    for i in range(1,n+1):
        x=a+i*dx
        s+=dx*f(x)
    return s

def trapez(f,a,b,n): # Bu da integral hesaplıyor
    if n<1 or a>b:
        print("Hatalı veri")
    else:
        dx=(b-a)/n # [a,b] aralığını parçalıyor
        s=0.5*(f(a)+f(b)) # aritmetik orta
        for i in range(1,n):
            x=a+i*dx
            s+=f(x) 
    return s*dx # 

#Örnekler--------------------------------
def g(x):
    return x**3
a=0;b=1
n=100 # parçalanış sayısı

print(integral(g,a,b,n))
print(trapez(g,a,b,n))
