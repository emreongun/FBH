﻿#-----------------------------------------------------------24.03.2015---6.Hafta

from math import*

def türevler(f,h,x): # birinci ve ikinci türev
    f1=(f(x+h)-f(x-h))/(2*h)
    f2=(f(x+h)-2*f(x)+f(x-h))/h**2
    print("İlk türev: ",f1,"\nİkinci türev: ",f2)

#Örnek----------------------------------------
def g(x): # türevi alınacak işlevi (fonksiyonu) buraya yazın
    return x**3 + x**2 # örnek bir polinom
h=0.1;x=2 # 'h' hassasiyetinde, 'x' noktasında
türevler(g,h,x)
