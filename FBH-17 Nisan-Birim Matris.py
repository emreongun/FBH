#-----------------------------------------------------------17.03.2015---5.Hafta

N=3			# NxN matris için
A=[]			# Üst listemiz
for i in range(0,N):
    B=[]		# Alt listemiz  
    for j in range(0,N):
        if i==j:	# Köşegen elemanların i ve j indisleri aynıdır.
            B.append(1)
        else:
            B.append(0)
    A.append(B)		# B satırı A'ya eklenir

for i in range(len(A)):
    print(A[i])

# İçini alt liste ile dolduracağımız bir üst liste oluşturduk.
# A üst listesine eklediğim B alt listeleri matrisinin satır elemanları oluyor.
# Çıktıda görünen alt listeler A matrisinin her bir satırı anlamına geliyor. 

