﻿#-----------------------------------------------------------14.04.2015---7.hafta

# Kenarları 1 birim uzunluğunda bir karenin içinde yarıçapı 1 olan çeyrek çember
#düşünelim. Bu karenin içinde seçilen rastgele bir (x,y) noktasının çemberin
#içinde olma olasılığını hesaplayalım.

def rastgele(jbaşla):
    ia=211;ib=1663;ic=7875
    jbaşla=(jbaşla*ia+ib)%ic
    return float(jbaşla)/float(ic),jbaşla

jbaşla=18
kare=10 # kare içine yerleştireceğimiz rast gele nokta sayımız
print("\nnokta sayısı","-","pi sayısı")
for i in range(4): # artan nokta sayısında iyileşen sonuçları görmek için
    idaire=0
    for j in range(0,kare): # karedeki bütün noktalar
        x,jbaşla=rastgele(jbaşla)
        y,jbaşla=rastgele(jbaşla)
        if (x**2+y**2) <= 1.0: # dairenin içinde ise
            idaire += 1 # dairedeki nokta sayısını bir arttır

    pisayısı=4*float(idaire)/kare # tam daire dört çeyrek eder.
    # daire alanı pi*yarıçap_kare
    # kare alanı uzunluk_kare
    # Bir birlerine oranı pi sayısıdır.

    print("%11d"%kare,"  ","%2.8f"%pisayısı)
    kare *= 10 # her döngüde kare içindeki nokta sayısını 10 katına çıkar

