﻿#----------------------------------------------------------05.05.2015---10.hafta
import random
"""
def montecarlo_t(f,a,b): # [a,b] aralığı montecarlo tekniği (tek katlı integral)
    sonuç=0; toplam=0 # başlangıç
    n=100000 # aralık sayısı

    for i in range(1,n+1):
        x=random.uniform(a,b)
        # x=random.random() # üst satır yerine bu satırlar da
        # x=(b-a)*x+a       # yazılabilir
        toplam += f(x)
    sonuç = (b-a)/n*toplam # a-b aralı ile çarpılmalı
    return sonuç
"""

def montecarlo(f,a,b,c,d): # [a,b] aralığı montecarlo tekniği (çok katlı integral)
    sonuç=0; toplam=0 # başlangıç
    n=100000 # aralık sayısı

    for i in range(1,n+1):
        x=random.uniform(a,b)
        y=random.uniform(c,d)
        toplam += f(x,y)
    sonuç = (b-a)*(d-c)/n*toplam # a-b aralı ile çarpılmalı
    return sonuç

def F(x): # rastgele bir işlev
    return x**2

def G(x,y):
    return x**2*y

a=0;b=1;c=2;d=4

metin="{0:s} işlevinin dx€[{2:d},{3:d}] ve dy€[{4:d},{5:d}] aralığında integrali: {1:3.4f}"
print(metin.format("G",montecarlo(G,a,b,c,d),a,b,c,d))
