#-------------------------------------------------------------------------------
# İsim:         Türev
# Amaç:         Belirli bir sayı için sinüs işlevinin ilk iki türevini alabilmek
# Yazar:        Emre ONGUN
# Oluşturma:    17.03.2015 --- 5.hafta
# Telif:        (c) Emre ONGUN 0402110017
# Lisasn:       GPLv3
#-------------------------------------------------------------------------------

# f'(x)=(f(x+h)-f(x))/h

# f''(x)=(f(x+h)-2.0*f(x)+f(x-h))/h**2

from math import sin
# sin yerine * koyarak kütüphanenin tamamı içe aktarılabilir.

def sin1(x,h):
    türev1=(sin(x+h)-sin(x))/h
    return türev1

def sin2(x,h):
    türev2=(sin(x+h)-2*sin(x)+sin(x-h))/h**2
    return türev2

x=float(input("X=")) # x değeri
h=float(input("h=")) # hassasiyet

print("x =",x,"için")
print("sin'({:3f}) =".format(x),sin1(x,h))
print("sin''({:3f}) =".format(x),sin2(x,h))

