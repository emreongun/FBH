﻿#-----------------------------------------------------------14.04.2015---7.hafta

def rastgele(jbaşla):
    ia=211;ib=1663;ic=7875 # iyi bir hesap için ideal sayılarmış
    jbaşla=(jbaşla*ia+ib)%ic
    return float(jbaşla)/float(ic),jbaşla # iki değer döndürüyor
    # döndürülen değerlerden ilki [0,1] aralığında rastgele bir sayı
    # diğer deper jbaşla değerini değiştirmek için başka bir sayı

tura=yazı=0 # yazı ve tura çıkan deney sonucumuz başlangıçta sıfır
jbaşla=18 # en iyi uyduruk sayı 18 imiş
n=int(input("Deney sayısı: ")) # deney sayısını çalışma sırasında siz belirleyin

# yazı tura oyununda yazı çıkma olasılığı
#[0,1] aralığında seçilen bir sayının 0.5'den küçük olma olasılığı ile aynıdır.
for i in range(0,n): # n kez
    # rastgele bir r sayısı üretiyor ve jbaşla değişiyor.
    r,jbaşla=rastgele(jbaşla)
    if r < 0.5:
        yazı=yazı+1 # yazı çıkyığında bir arttır

tura=n-yazı # tura sayısı = deney - yazı sayısı
print("""{0:n} atışta 
tura çıkma olasılığı %{2:4.2f}
yazı çıkma olasılığı %{1:4.2f}
""".format(n,100*yazı/n,100*tura/n))
